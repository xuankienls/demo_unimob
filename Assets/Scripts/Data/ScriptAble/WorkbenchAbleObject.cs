using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Workbend", menuName = "Assets/data")]
public class WorkbenchAbleObject : ScriptableObject
{
    public WorkbenchModel[] workbench;
}

[Serializable]
public struct WorkbenchModel
{
    public string id;
    public GameObject model;
}
