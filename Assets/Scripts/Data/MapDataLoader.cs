using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class MapDataLoader : IDataLoader
{
    public async Task LoadData()
    {
      GameData.workbench.Clear();
        var loader = await DataLoader.LoadAndParseFromTextAsset<MapDataEntity>(GameDataJson.Instance.MapData);
        if (false == string.IsNullOrEmpty(loader.errorMessage))
        {
            Debug.LogError($" load and parse json error: {loader.errorMessage}");
        }
        foreach (var map in loader.data.workbench)
        {
            GameData.workbench.Add(map);
        }
    }
}
