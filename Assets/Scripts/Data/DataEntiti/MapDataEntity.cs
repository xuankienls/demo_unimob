using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct MapDataEntity 
{
    public List< WorkbendDataEntity> workbench;
}


[Serializable]
public struct WorkbendDataEntity
{
    public string id;
    public string name;
    public StatusWorkbench statusWorkbench;
    public float[] position;
    public int coin;

}

public enum StatusWorkbench
{
    build,
    work
}
