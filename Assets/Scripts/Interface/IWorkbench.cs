
public interface IWorkbench 
{
    void Init(WorkbendDataEntity workbendDataEntity);
    void InteractCharactor(ICharactor charactor, bool isPlayer, bool isEnter);
}
