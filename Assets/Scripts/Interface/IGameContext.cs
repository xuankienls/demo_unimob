using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameContext 
{
    void OnStartContext();
}
