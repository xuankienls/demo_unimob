
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface ICharactor 
{
    bool IsFull();
    void GetItem(GameObject items);
    int CountItem();
    List<GameObject> PushAllItem(); 
    GameObject PushItem();

    GameObject GetThis();
    void Move(Vector3 pos, float time, UnityAction cb = null);
}
