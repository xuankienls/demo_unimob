using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class MainContext : MonoBehaviour
{
    private IDataLoader[] _dataLoader;
    private IGameContext[] _gameContext;
    [SerializeField] private NavMeshSurface meshSurface;
    private void Awake()
    {
        ObjectPoolExt.Init(GetComponent<IObjectPool>());
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = 60;
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
        StartGame();
        PlayerData.Money = 100;
    }

    void Init()
    {
        _dataLoader = new IDataLoader[]
        {
            new MapDataLoader()
        };
        _gameContext = GetComponents<IGameContext>();
    }

   async void StartGame()
    {
        foreach (IDataLoader loader in _dataLoader)
        {
            await loader.LoadData();
        }

        await Task.Delay(200);

        foreach (var context in _gameContext)
        {
            context.OnStartContext();
        }

        await Task.Delay(500);
    }
}
