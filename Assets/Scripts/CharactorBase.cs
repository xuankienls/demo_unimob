using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharactorBase : MonoBehaviour, ICharactor
{
    protected float speed = 0.1f;
    [SerializeField] protected Rigidbody rb;
   [SerializeField] protected Animator animator;
    [SerializeField] protected Transform model;
    [SerializeField] protected Transform parentItem;
    [SerializeField] protected int max = 5;
    [SerializeField] protected List<GameObject> lstTomato = new List<GameObject>();
    public UnityAction actionMoveDone = null;
    bool _isMove;
   public bool isMove
    {
        get => _isMove;
        set
        {
            if (_isMove != value)
            {
                _isMove = value;
                animator.SetBool(AnimCommand.IsMove, isMove);
                animator.SetBool(AnimCommand.IsCarryMove, isMove);
            }
        }
    }

    public bool IsFull()
    {
        if (lstTomato.Count >= max)
            return true;
        return false;
    }

    public virtual void GetItem(GameObject items)
    {
        if (items != null)
        {
            lstTomato.Add(items);
            items.transform.parent = parentItem;
            SetPositionItems();
            animator.SetBool(AnimCommand.IsEmpty, !(lstTomato.Count > 0));
        }
    }

    void SetPositionItems()
    {
        for (int i = 0; i < lstTomato.Count; i++)
        {
            lstTomato[i].transform.DOLocalMove(Vector3.up * 0.2f * i, 0.5f).SetEase(Ease.Linear);
        }
    }

    public virtual int CountItem()
    {
        return lstTomato.Count;
    }
    public virtual GameObject PushItem()
    {
        if (lstTomato.Count < 0) return null;
        GameObject itemsClone = lstTomato[lstTomato.Count - 1];
        lstTomato.Remove(itemsClone);
        animator.SetBool(AnimCommand.IsEmpty, !(lstTomato.Count > 0));
        return itemsClone;
    }

    public virtual List<GameObject> PushAllItem()
    {
        List<GameObject> allItem = new List<GameObject>();
        allItem.AddRange(lstTomato);
        lstTomato.Clear();
        animator.SetBool(AnimCommand.IsEmpty, !(lstTomato.Count > 0));
        return allItem;
    }

    public void Move(Vector3 pos, float time, UnityAction cb = null)
    {
        transform.LookAt(pos);
      transform.DOMove(pos, time).SetEase(Ease.Linear).OnComplete(() => {
          isMove = false;
          cb?.Invoke(); });
    }

    public GameObject GetThis()
    {
        return gameObject;
    }
}
