using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class BuildingWorkbench : MonoBehaviour, IWorkbench
{
    [SerializeField] TextMeshPro money;
    [SerializeField] int coin;
    [SerializeField] Transform floor;
    [SerializeField] WorkbendDataEntity _workbendDataEntity;

    private void Awake()
    {
        floor.DOScale(floor.transform.localScale*1.2F,0.3f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
    }
    public void Init(WorkbendDataEntity workbendDataEntity)
    {
        _workbendDataEntity = workbendDataEntity;
        coin = _workbendDataEntity.coin;
        money.text = coin.ToString();
    }


    public void InteractCharactor(ICharactor charactor, bool isPlayer, bool onEnter)
    {
        if (isPlayer)
        {
            isActive = onEnter;
            if(onEnter)
            if (coin <= 0)
            {
                Build();
            }
        }
    }

    void Build()
    {
        _workbendDataEntity.statusWorkbench = StatusWorkbench.work;
        _workbendDataEntity.BuildWorkbend();
        gameObject.Recycle();
    }

    void UpdateMoney()
    {
        if(PlayerData.Money > 0)
        {
            coin --;
            PlayerData.Money--;
            money.text = coin.ToString();
            if(coin <= 0)
            {
                Build();
            }
        }
    }

    bool isActive = false;
    float timeResource = 0.05f;
    float timeCount = 0;
    // Update is called once per frame
    void Update()
    {
        if (isActive && coin > 0 && _workbendDataEntity.statusWorkbench == StatusWorkbench.build)
        {
            if (timeCount < timeResource)
            {
                timeCount += Time.deltaTime;
                return;
            }
            timeCount = 0;
            UpdateMoney();
        }
    }
}
