using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UIElements;

public class CashierWorkbench : MonoBehaviour, IWorkbench
{
    public Transform posPay;
    [SerializeField] GameObject money;
    [SerializeField] Transform posMoney;
    [SerializeField] GameObject box;
    [SerializeField] Transform posBox, floor;
    [SerializeField] WorkbendDataEntity _workbendDataEntity;
    [SerializeField] List<GameObject> listMoney = new List<GameObject>();
    public List<ICharactor> _client = new List<ICharactor>();
    List<ICharactor> _Player = new List<ICharactor>();

    Vector3 scaleFloor;
    private void Awake()
    {
        scaleFloor = floor.localScale;
    }
    public void Init(WorkbendDataEntity workbendDataEntity)
    {
        _workbendDataEntity = workbendDataEntity;
        ClientManager.instance.cashierWorkbench = this;
        box.CreatePool();
        money.CreatePool();
    }

    public void InteractCharactor(ICharactor charactor, bool isPlayer, bool isEnter)
    {
        if (isPlayer)
        {
            if (isEnter)
            {
                if (!_Player.Contains(charactor))
                    _Player.Add(charactor);
                Pay();
                CheckMoney();
                floor.DOScale(scaleFloor * 1.2f, 0.2f).SetEase(Ease.OutCirc);
            }
            else
            {
                floor.DOScale(scaleFloor, 0.2f).SetEase(Ease.InCirc);
                if (_Player.Contains(charactor))
                    _Player.Remove(charactor);
            }
        }
    }

    public void AddClient(ICharactor charactor)
    {
       if(!_client.Contains(charactor))
        {
            _client.Add(charactor);
        }
        CheckClientMove();
    }

    public void RemoveClient(ICharactor charactor)
    {
        if (_client.Contains(charactor))
        {
            _client.Remove(charactor);
        }
        CheckClientMove();
    }

    public void CheckClientMove()
    {
        for (int i = 0; i < _client.Count; i++)
        {
            Vector3 pos = posPay.position + Vector3.right*i;
            _client[i].Move(pos, 1, i==0? Pay: null);

        }
    }
    ICharactor clientPay ;

    void Pay()
    {
        if (_client.Count>0 && _Player.Count > 0)
        {
            if (clientPay == _client[0]) return;
            if (Vector3.Distance(_client[0].GetThis().transform.position, posPay.position) > 0.5f) return;
            clientPay = _client[0];

            box.Use(out var boxClone);
            boxClone.transform.SetParent(transform);
            boxClone.transform.localRotation = Quaternion.Euler(0,0,0);
            boxClone.transform.position = posBox.position;
            List<GameObject> lstItem = clientPay.PushAllItem();
            int numberitem = lstItem.Count*3;

            foreach (var item in lstItem)
            {
                item.transform.DOMove(posBox.position, 0.3f).SetEase(Ease.Linear).OnComplete(() => { item.Recycle(); });
            }
            DOVirtual.DelayedCall(0.6f, () =>
            {
                for (int i = 0; i < numberitem; i++)
                {
                    money.Use(out var moneyClone);
                    Vector3 posCurrentMoney = clientPay.GetThis().transform.position;
                    posCurrentMoney.y = posMoney.position.y;
                    moneyClone.transform.position = posCurrentMoney;
                    moneyClone.transform.DOMove(  posMoney.position + Vector3.right*0.15f*(i%10) + Vector3.forward * 0.15f * Mathf.FloorToInt( i/10),0.5f).SetDelay(i*0.1f).SetEase(Ease.Linear);
                    listMoney.Add(moneyClone);
                }
                CheckMoney();


                clientPay.GetThis().GetComponent<Client>().GetBox(boxClone);
                RemoveClient(clientPay);
            });
        }
    }

    void CheckMoney()
    {
        if(listMoney.Count > 0 && _Player.Count > 0)
        {
            PlayerData.Money += listMoney.Count;
            int count = 0;
            foreach (var moneyClone in listMoney)
            {
                Vector3 posEndMoney = _Player[0].GetThis().transform.position;
                posEndMoney.y = moneyClone.transform.position.y;
                moneyClone.transform.DOMove(posEndMoney, 0.3f).SetDelay(count * 0.1f).SetEase(Ease.Linear).OnComplete(() => { moneyClone.Recycle(); });
                count++;

            }
        }
    }

}
