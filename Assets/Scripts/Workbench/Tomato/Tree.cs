using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class Tree : MonoBehaviour, IWorkbench
{
    [SerializeField] List< ItemInTree> _itemTree = new List<ItemInTree>();
    [SerializeField] Transform pointParent;
    [SerializeField] WorkbendDataEntity _workbendDataEntity;
    [SerializeField] GameObject tomato;

    List<ICharactor> _charactors = new List<ICharactor>();
    void Start()
    {
        _itemTree.AddRange(pointParent.GetComponentsInChildren<ItemInTree>());
        tomato.CreatePool();
    }

    public void Init(WorkbendDataEntity workbendDataEntity)
    {
        _workbendDataEntity = workbendDataEntity;
    }

    public void InteractCharactor(ICharactor charactor, bool isPlayer, bool isEnter)
    {
        if (isPlayer)
        {
            if (isEnter)
            {
                if(!_charactors.Contains(charactor))
                    _charactors.Add(charactor);
                ClientGetTomato();
            }
            else
            {
                if (_charactors.Contains(charactor))
                    _charactors.Remove(charactor);
            }
        }
    }
  
    void ClientGetTomato()
    {
        foreach (var charactor in _charactors)
        {
            if (charactor != null) {
                foreach (ItemInTree item in _itemTree)
                {
                    if (charactor.IsFull()) break;
                    if (item.HaveItem())
                        charactor.GetItem(item.GetItem());
                }
            }
        }
    }

    void SpawnTomato()
    {
        foreach(ItemInTree item in _itemTree)
        {
            if (false == item.HaveItem())
            {
                tomato.Use(out var tomatoClone);
                tomatoClone.transform.position = transform.position;
                item.AddItem(tomatoClone);
                break;
            }
        }
        ClientGetTomato();

    }

    float timeResource = 3f;
    float timeCount = 0;
    private void Update()
    {
        if(timeCount < timeResource)
        {
            timeCount += Time.deltaTime;
            return;
        }
        timeCount = 0;
        SpawnTomato();
    }


}
