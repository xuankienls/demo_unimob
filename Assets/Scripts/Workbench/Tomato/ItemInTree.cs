using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInTree : MonoBehaviour
{
    [SerializeField] GameObject _itemTomato;
    public GameObject ItemTomato { get { return _itemTomato; } }

    public bool HaveItem()
    {
        return _itemTomato != null;
    }

    public GameObject GetItem() {
        GameObject item = _itemTomato;
        _itemTomato = null;
        return item;
    }

    public void AddItem(GameObject itemTomato)
    {
        if (_itemTomato != null || itemTomato == null) return;
        _itemTomato = itemTomato;
        _itemTomato.transform.SetParent(transform);
        _itemTomato.transform.DOLocalMove(Vector3.zero,0.5f).SetEase(Ease.Linear);

    }
}
