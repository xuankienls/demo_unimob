using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketWorkbench : MonoBehaviour, IWorkbench
{
    [SerializeField] WorkbendDataEntity _workbendDataEntity;
    List<ICharactor> _lstClient = new List<ICharactor>();
    int max = 15;
    [SerializeField] Vector2 ratioPosItem = new Vector2(0.3f, 0.3f);
    [SerializeField] Transform posItem, floor;
    public List<Transform> posWaiting;
    [SerializeField] Queue<GameObject> lstItem = new Queue<GameObject>();

    Vector3 scaleFloor;
    private void Awake()
    {
        scaleFloor = floor.localScale;
    }

    public void Init(WorkbendDataEntity workbendDataEntity)
    {
        _workbendDataEntity = workbendDataEntity;
        ClientManager.instance.marketWorkbench.Add(this);
    }

    int index = 0;
    public Vector3 GetPosWaiting()
    {
        Vector3 pos = posWaiting[index].position;
        index++;
        if (index >= posWaiting.Count) index = 0;
        return pos;
    }

    public void InteractCharactor(ICharactor charactor, bool isPlayer, bool isEnter)
    {
        if (isPlayer )
        {
            if(isEnter)
            {
                int duphong = 0;
                while (lstItem.Count < max && charactor.CountItem() > 0) {
                    GameObject itemClone = charactor.PushItem();
                    itemClone.transform.SetParent(posItem);
                    itemClone.transform.DOLocalMove(Vector3.right * ratioPosItem.x * (lstItem.Count % 5) + Vector3.forward * ratioPosItem.y * Mathf.FloorToInt(lstItem.Count / 5), 0.5f);
                    lstItem.Enqueue(itemClone);

                    ClientGetTomato();
                    duphong++;
                    if (duphong > max) return;
                }
                floor.DOScale(scaleFloor * 1.2f, 0.2f).SetEase(Ease.OutCirc);
            }
            else
            {
                floor.DOScale(scaleFloor, 0.2f).SetEase(Ease.InCirc);
            }
        }
        else
        {
            if (isEnter)
            {
                if (!_lstClient.Contains(charactor))
                    _lstClient.Add(charactor);
                ClientGetTomato();
            }
            else
            {
                if (_lstClient.Contains(charactor))
                    _lstClient.Remove(charactor);
            }
        }
    }

    void ClientGetTomato()
    {
            foreach (var client in _lstClient)
            {
                if (client != null)
                {
                if (lstItem.Count > 0)
                {
                    if (!client.IsFull())
                    {
                        client.GetItem(lstItem.Dequeue());

                    }
                }
                else
                {
                    return;
                }
                }
            }
        
    }
}
