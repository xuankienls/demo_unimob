using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CharactorBase
{
    [SerializeField] DynamicJoystick joystick;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        isMove = false;
        animator.SetBool(AnimCommand.IsEmpty, true);
        animator.SetBool(AnimCommand.IsMove, isMove);
        animator.SetBool(AnimCommand.IsCarryMove, false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("workbench"))
        {
            other.transform.parent.GetComponent<IWorkbench>().InteractCharactor(this, true, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("workbench"))
        {
            other.transform.parent.GetComponent<IWorkbench>().InteractCharactor(this, true, false);
        }
    }

    public float getAngle(Vector2 me, Vector2 target)
    {
        return (float)(Math.Atan2(target.y - me.y, target.x - me.x) * (180 / Math.PI));
    }
    public void FixedUpdate()
    {

        Vector3 direction = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal;
        isMove = (joystick.Vertical != 0 || joystick.Horizontal != 0);
        if (isMove)
        {
            model.localRotation = Quaternion.Euler(Vector3.up * -(getAngle(Vector2.zero, Vector2.up * joystick.Vertical + Vector2.right * joystick.Horizontal) - 90));
        }
        transform.position += direction * speed;
    }

}
