using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerData 
{
        
    public static int Money
    {
        get => PlayerPrefs.GetInt(DataCommand.money, 0);
        set
        {
            int money = value;
            PlayerPrefs.SetInt(DataCommand.money, money);
            Observer.Instance.Notify(ObserverKey.ChangeMoney);
        }
    }
}

public class DataCommand
{
    public const string money = "money";
}

public class AnimCommand
{
    public const string IsMove = "IsMove";
    public const string IsCarryMove = "IsCarryMove";
    public const string IsEmpty = "IsEmpty";
}
