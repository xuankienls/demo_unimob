using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WorkbenchExts 
{
    private static WorkbenchSystem _workbenchSystem;
    public static void Init(WorkbenchSystem workbenchSystem) => _workbenchSystem = workbenchSystem;
    public static GameObject GetModelWorkbend(this WorkbendDataEntity workbendData)
    {
        if (workbendData.statusWorkbench == StatusWorkbench.build)
        {
          return  _workbenchSystem.GetModel("building");
        }
        return _workbenchSystem.GetModel(workbendData.id);
    }

    public static void BuildWorkbend(this WorkbendDataEntity workbendData)
    {
        GameObject workbenchClone = workbendData.GetModelWorkbend();
        workbenchClone.Use(out GameObject _workbench);
        _workbench.name = workbendData.name;
        IWorkbench workbench = _workbench.GetComponent<IWorkbench>();
        if (workbench != null ) workbench.Init(workbendData);
        _workbench.transform.position = new Vector3(workbendData.position[0], workbendData.position[1], workbendData.position[2]);

    }
}
