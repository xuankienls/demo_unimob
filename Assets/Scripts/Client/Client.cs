using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Client : CharactorBase
{

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update

    void ResetAll()
    {
        isMove = false;
        animator.SetBool(AnimCommand.IsEmpty, true);
        animator.SetBool(AnimCommand.IsMove, isMove);
        animator.SetBool(AnimCommand.IsCarryMove, false);
        foreach (var t in lstTomato)
        {
            t.Recycle();
        }
        lstTomato.Clear();
    }

    public void Setup(Vector3 posSpawn, Vector3 posMove)
    {
        ResetAll();
        max = Random.Range(2, 5);
        transform.position = posSpawn;
        isMove = true;
        animator.SetBool(AnimCommand.IsMove, true);
        transform.LookAt(posMove);
        transform.DOMove(posMove, 5).SetEase(Ease.Linear).OnComplete( () =>
        {
            animator.SetBool(AnimCommand.IsMove, false);
        });
     //   agent.SetDestination(posMove);
    }

    public override void GetItem(GameObject items)
    {
        base.GetItem(items);
        if(parentItem.childCount > 0)
        {
            animator.SetBool(AnimCommand.IsEmpty, false);
        }
        DOVirtual.DelayedCall(0.5f, () =>
        {
            if (IsFull())
            {
                CashierWorkbench cashier = ClientManager.instance.cashierWorkbench;
                cashier.AddClient(this);
            }
        });

    }

    public void GetBox(GameObject box)
    {
        animator.SetBool(AnimCommand.IsEmpty, false);
        animator.SetBool(AnimCommand.IsCarryMove, true);
        box.transform.parent = parentItem;
        box.transform.DOLocalMove(Vector3.zero, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            transform.DOMove(ClientManager.instance.transform.position, 5).SetEase(Ease.Linear).OnComplete(() => {
                box.Recycle();
                ClientManager.instance.RecycleClient(this.gameObject); });
        });
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("workbench"))
        {
            other.transform.parent.GetComponent<IWorkbench>().InteractCharactor(this, false, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("workbench"))
        {
            other.transform.parent.GetComponent<IWorkbench>().InteractCharactor(this, false, false);
        }
    }


}
