using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientManager : MonoBehaviour
{
    public static ClientManager instance;
    public Client client;
    public List<GameObject> clients = new List<GameObject>();
    public CashierWorkbench cashierWorkbench;
    public List<MarketWorkbench> marketWorkbench = new List<MarketWorkbench>();
    bool isRegisterClientToPool;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        client.gameObject.CreatePool();
        isRegisterClientToPool = true;
    }

    void SpawnClient()
    {
        if (clients.Count > 4) return;

        client.gameObject.Use(out var ClientClone);
        ClientClone.GetComponent<Client>().Setup(transform.position, marketWorkbench[0].GetPosWaiting());
        clients.Add(ClientClone);

    }

    public void RecycleClient(GameObject clientRecycle)
    {
        if(clients.Contains(clientRecycle))
        {
            clients.Remove(clientRecycle);
        }
        clientRecycle.Recycle();
    }

    float timeResource = 5f;
    float timeCount = 5;
    private void Update()
    {
        if (!isRegisterClientToPool) return;
        if (timeCount < timeResource)
        {
            timeCount += Time.deltaTime;
            return;
        }
        if (marketWorkbench.Count > 0)
        {

            timeCount = 0;
            SpawnClient();
        }
    }
}
