using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkbenchSystem : MonoBehaviour
{
    [SerializeField] WorkbenchAbleObject _dataAsset;
    Dictionary<string, GameObject> lstDataAsset = new Dictionary<string, GameObject>();

    private void Awake()
    {
        WorkbenchExts.Init(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in _dataAsset.workbench)
        {
            if( item.model !=  null)
            {
                if (!lstDataAsset.ContainsKey(item.id))
                {
                    lstDataAsset.Add(item.id, item.model);
                    item.model.CreatePool();
                }
                else
                    Debug.LogError($"multi id workbench with id: {item.id}");
            }
            else
            {
                Debug.LogError($"workbench with id: {item.id} null");
            }
        }
    }

    public GameObject GetModel(string id)
    {
        if (lstDataAsset.ContainsKey(id) && lstDataAsset[id] != null) return lstDataAsset[id];
        Debug.LogError($"not find workbench {id}");
        return null;
    }
}
