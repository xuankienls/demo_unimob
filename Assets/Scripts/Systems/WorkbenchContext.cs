using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkbenchContext : MonoBehaviour, IGameContext
{
    public void OnStartContext()
    {
        foreach (var workbench in GameData.workbench)
        {
           workbench.BuildWorkbend();
        }
    }


}
