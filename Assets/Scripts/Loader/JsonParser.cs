using System;
using Newtonsoft.Json;

public struct JsonParser
{
    public string ErrorMessage { get; private set; }

    public bool TryParse<T>(string jsonText, out T result)
    {
        result = default;
        ErrorMessage = string.Empty;

        try
        {
            result = JsonConvert.DeserializeObject<T>(jsonText);
            return true;
        }
        catch (Exception e)
        {
            ErrorMessage = e.Message;
        }

        return false;
    }

    public bool TrySerialize<T>(T data, out string json)
    {
        json = string.Empty;
        ErrorMessage = string.Empty;

        try
        {
            json = JsonConvert.SerializeObject(data);
            return true;
        }
        catch (Exception e)
        {
            ErrorMessage = e.Message;
        }

        return false;
    }
}