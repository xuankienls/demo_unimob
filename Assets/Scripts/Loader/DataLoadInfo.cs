using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public readonly struct DataLoadInfo<T>
{
    public readonly string errorMessage;
    public readonly T data;

    public DataLoadInfo(T resultData, string error = null)
    {
        this.errorMessage = error;
        this.data = resultData;
    }
}
