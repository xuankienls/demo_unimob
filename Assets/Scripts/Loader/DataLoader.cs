
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;



public static class DataLoader 
{
    private static bool _init;

    public static async Task<DataLoadInfo<T>> LoadAndParseFromTextAsset<T>(TextAsset textAsset)
    {
        if (textAsset == null) return new DataLoadInfo<T>(default, "text asset null");

        var jsonParse = new JsonParser();
        if (jsonParse.TryParse<T>(textAsset.text, out var result) == false)
            return new DataLoadInfo<T>(result, $"parse json from textasset {textAsset.name} fail!");
        return new DataLoadInfo<T>(result);
    }
}
