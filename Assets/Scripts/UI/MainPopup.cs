using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainPopup : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI moneyTxt;
    int valueMoney;
    // Start is called before the first frame update
    void Start()
    {
        valueMoney = PlayerData.Money;
        moneyTxt.text = valueMoney.ToString();
        Observer.Instance.AddObserver(ObserverKey.ChangeMoney, OnChangeMonet);
    }

    private void OnDestroy()
    {
        if(Observer.Instance != null)
        {
            Observer.Instance.RemoveObserver(ObserverKey.ChangeMoney, OnChangeMonet);
        }
    }

    void OnChangeMonet(object data = null)
    {
        if(valueMoney != PlayerData.Money)
        {
            DOVirtual.Float(valueMoney, PlayerData.Money, 0.5f, (value) =>
            {
                valueMoney =(int) value;
                moneyTxt.text = valueMoney.ToString();
            });

        }
    }

   public int test = 10;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.A)) { 
        PlayerData.Money += test;
        }
    }
}
